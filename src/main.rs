use aoc::client::Client;
use clap::Parser;
use std::env;

mod puzzles;
use puzzles::*;

#[derive(Parser)]
struct Args {
    #[arg(short, long)]
    day: u8,

    #[arg(short, long)]
    year: u32,

    #[arg(long, default_value_t = false)]
    submit: bool,

    #[arg(long, default_value_t = false)]
    part1_only: bool,

    #[arg(long, default_value_t = false)]
    part2_only: bool,
}

fn execute_parts(client: &Client, args: &Args) {
    let mut implementation = utils::get_day(args.year, args.day);
    let input = client
        .get_input(args.day)
        .unwrap_or_else(|_| panic!("Failed to get input for day {}", args.day));

    let restrict_parts = args.part1_only || args.part2_only;

    for part in 1..=2 {
        if restrict_parts {
            if part == 1 && !args.part1_only {
                continue;
            }
            if part == 2 && !args.part2_only {
                continue;
            }
        }

        let solution = match part {
            1 => {
                implementation.init_part1();
                implementation.call_part1(&input)
            }
            2 => {
                implementation.init_part2();
                implementation.call_part2(&input)
            }
            p => panic!("Unknown part number {p}!"),
        };

        println!("Solution to part {part}: {solution}");
        if args.submit {
            client
                .submit_solution(args.day, part, &solution)
                .expect("Failed to submit solution for day {day} part {part}");
        }
    }
}

fn main() {
    let args = Args::parse();
    let token = env::var("AOC_SESSION_TOKEN")
        .expect("Could not find environment variable 'AOC_SESSION_TOKEN'");

    let client = Client::new(args.year.to_string(), token).unwrap();

    execute_parts(&client, &args);
}
