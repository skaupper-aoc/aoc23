use crate::utils::DayTrait;
use std::cmp::max;

#[derive(Default)]
pub struct Implementation {
    max_red: u32,
    max_green: u32,
    max_blue: u32,
}

impl DayTrait for Implementation {
    fn init_part1(&mut self) {
        self.max_red = 12;
        self.max_green = 13;
        self.max_blue = 14;
    }

    fn call_part1(&self, input: &str) -> String {
        let lines = input.lines().map(str::trim).filter(|l| !l.is_empty());

        let mut sum = 0;

        for line in lines {
            let game = Game::try_from(line).unwrap();

            let is_game_possible = game.draws.iter().all(|draw| {
                draw.0 <= self.max_red && draw.1 <= self.max_green && draw.2 <= self.max_blue
            });
            if is_game_possible {
                sum += game.id;
            }
        }

        sum.to_string()
    }

    fn call_part2(&self, input: &str) -> String {
        let lines = input.lines().map(str::trim).filter(|l| !l.is_empty());

        let mut sum = 0;

        for line in lines {
            let game = Game::try_from(line).unwrap();

            let mut min_cubes_possible = Draw(0, 0, 0);
            game.draws.iter().for_each(|draw| {
                min_cubes_possible.0 = max(min_cubes_possible.0, draw.0);
                min_cubes_possible.1 = max(min_cubes_possible.1, draw.1);
                min_cubes_possible.2 = max(min_cubes_possible.2, draw.2);
            });

            sum += min_cubes_possible.0 * min_cubes_possible.1 * min_cubes_possible.2;
        }

        sum.to_string()
    }
}

struct Draw(u32, u32, u32);

struct Game {
    id: u32,
    draws: Vec<Draw>,
}

impl TryFrom<&str> for Game {
    type Error = &'static str;

    fn try_from(input: &str) -> Result<Self, Self::Error> {
        let s = input.trim();

        // Remove 'Game ' prefix
        let s_opt = s.strip_prefix("Game ");
        if s_opt.is_none() {
            return Err("Game string must start with the literal 'Game '");
        }

        // Parse game ID
        let split = s_opt.unwrap().split_once(':');
        if split.is_none() {
            return Err("Game string did not include colon as expected!");
        }
        let (id_str, s) = split.unwrap();
        let id = id_str.parse().unwrap();

        // Parse draws
        let draws = s
            .split(';')
            .map(str::trim)
            .map(Draw::try_from)
            .collect::<Result<Vec<_>, _>>()?;

        Ok(Game { id, draws })
    }
}

impl TryFrom<&str> for Draw {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let mut draw = Draw(0, 0, 0);

        let components = value
            .split(',')
            .map(str::trim)
            .map(|s| s.split_once(' ').ok_or("Failed to split draw component!"));

        for splits in components {
            let (amount, color) = splits?;
            let amount = amount.parse().map_err(|_| "Failed to parse cube amount!")?;

            match color {
                "red" => draw.0 = amount,
                "green" => draw.1 = amount,
                "blue" => draw.2 = amount,
                _ => return Err(""),
            }
        }

        Ok(draw)
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
        Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
        Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
        Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
        Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
    ";

    #[test]
    fn test_day2_part1_example1() {
        let mut implementation = Implementation::default();
        implementation.init_part1();
        assert_eq!(implementation.call_part1(EXAMPLE_1), "8");
    }

    #[test]
    fn test_day2_part2_example1() {
        let implementation = Implementation::default();
        assert_eq!(implementation.call_part2(EXAMPLE_1), "2286");
    }
}
