use std::{
    collections::{HashMap, HashSet, VecDeque},
    str::FromStr,
};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let map = Map::from_str(input.trim()).unwrap();
        let points = map.simulate_beam(Beam {
            point: Point { x: 0, y: 0 },
            dir: Vector { x: 1, y: 0 },
        });

        points.len().to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let map = Map::from_str(input.trim()).unwrap();

        let vertical_start_beams = (0..map.width).flat_map(|x| {
            vec![
                Beam {
                    point: Point { x, y: 0 },
                    dir: Vector { x: 0, y: 1 },
                },
                Beam {
                    point: Point {
                        x,
                        y: map.height - 1,
                    },
                    dir: Vector { x: 0, y: -1 },
                },
            ]
        });
        let horizontal_start_beams = (0..map.height).flat_map(|y| {
            vec![
                Beam {
                    point: Point { x: 0, y },
                    dir: Vector { x: 1, y: 0 },
                },
                Beam {
                    point: Point {
                        x: map.width - 1,
                        y,
                    },
                    dir: Vector { x: -1, y: 0 },
                },
            ]
        });

        let energy_levels = vertical_start_beams
            .chain(horizontal_start_beams)
            .map(|beam| map.simulate_beam(beam))
            .map(|points| points.len());

        energy_levels.max().unwrap().to_string()
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
enum Tile {
    Empty,
    VerticalSplitter,
    HorizontalSplitter,
    MirrorClockwise,
    MirrorCounterClockwise,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Vector {
    x: i32,
    y: i32,
}

#[derive(PartialEq, Eq, Clone)]
struct Map {
    map: HashMap<Point, Tile>,
    width: usize,
    height: usize,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Beam {
    point: Point,
    dir: Vector,
}

impl TryFrom<char> for Tile {
    type Error = &'static str;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        Ok(match value {
            '.' => Tile::Empty,
            '/' => Tile::MirrorClockwise,
            '\\' => Tile::MirrorCounterClockwise,
            '|' => Tile::VerticalSplitter,
            '-' => Tile::HorizontalSplitter,
            _ => Err("Cannot parse tile")?,
        })
    }
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let map = HashMap::from_iter(s.trim().lines().enumerate().flat_map(|(y, line)| {
            line.trim()
                .chars()
                .enumerate()
                .map(move |(x, ch)| (Point { x, y }, Tile::try_from(ch).unwrap()))
        }));

        let height = map.keys().map(|p| p.y).max().unwrap() + 1;
        let width = map.keys().map(|p| p.x).max().unwrap() + 1;

        Ok(Map { map, width, height })
    }
}

impl std::ops::Add<Vector> for Point {
    type Output = Point;

    fn add(self, rhs: Vector) -> Self::Output {
        Point {
            x: ((self.x as i32) + rhs.x) as usize,
            y: ((self.y as i32) + rhs.y) as usize,
        }
    }
}

impl Vector {
    fn is_horizontal(&self) -> bool {
        self.x != 0
    }
    fn is_vertical(&self) -> bool {
        self.y != 0
    }

    fn rotate_clockwise(self) -> Vector {
        Vector {
            x: -self.y,
            y: -self.x,
        }
    }

    fn rotate_counter_clockwise(self) -> Vector {
        Vector {
            x: self.y,
            y: self.x,
        }
    }
}

impl Map {
    fn simulate_beam(&self, start_beam: Beam) -> HashSet<Point> {
        let mut to_handle = VecDeque::new();
        let mut handled = HashSet::new();

        to_handle.push_back(start_beam);

        while !to_handle.is_empty() {
            let beam = to_handle.pop_front().unwrap();
            if beam.point.x >= self.width || beam.point.y >= self.height || handled.contains(&beam)
            {
                continue;
            }
            handled.insert(beam);

            let tile = self.map[&beam.point];

            match tile {
                Tile::Empty => to_handle.push_back(Beam {
                    point: beam.point + beam.dir,
                    dir: beam.dir,
                }),
                Tile::VerticalSplitter => {
                    if beam.dir.is_vertical() {
                        to_handle.push_back(Beam {
                            point: beam.point + beam.dir,
                            dir: beam.dir,
                        });
                    } else {
                        let dir = beam.dir.rotate_clockwise();
                        to_handle.push_back(Beam {
                            point: beam.point + dir,
                            dir,
                        });
                        let dir = beam.dir.rotate_counter_clockwise();
                        to_handle.push_back(Beam {
                            point: beam.point + dir,
                            dir,
                        });
                    }
                }
                Tile::HorizontalSplitter => {
                    if beam.dir.is_horizontal() {
                        to_handle.push_back(Beam {
                            point: beam.point + beam.dir,
                            dir: beam.dir,
                        });
                    } else {
                        let dir = beam.dir.rotate_clockwise();
                        to_handle.push_back(Beam {
                            point: beam.point + dir,
                            dir,
                        });
                        let dir = beam.dir.rotate_counter_clockwise();
                        to_handle.push_back(Beam {
                            point: beam.point + dir,
                            dir,
                        });
                    }
                }
                Tile::MirrorClockwise => {
                    let dir = beam.dir.rotate_clockwise();
                    to_handle.push_back(Beam {
                        point: beam.point + dir,
                        dir,
                    });
                }
                Tile::MirrorCounterClockwise => {
                    let dir = beam.dir.rotate_counter_clockwise();
                    to_handle.push_back(Beam {
                        point: beam.point + dir,
                        dir,
                    });
                }
            }
        }

        handled.into_iter().map(|beam| beam.point).collect()
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = r"
        .|...\....
        |.-.\.....
        .....|-...
        ........|.
        ..........
        .........\
        ..../.\\..
        .-.-/..|..
        .|....-|.\
        ..//.|....
    ";

    #[test]
    fn test_day16_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "46");
    }

    #[test]
    fn test_day16_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "51");
    }
}
