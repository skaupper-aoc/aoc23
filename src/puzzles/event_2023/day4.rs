use std::{
    collections::{HashMap, HashSet},
    ops::RangeInclusive,
};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        input
            .lines()
            .map(str::trim)
            .filter(|l| !l.is_empty())
            .map(ScratchCard::try_from)
            .map(Result::unwrap)
            .map(|sc| sc.count_matching_numbers())
            .map(|n| if n > 0 { 2u32.pow((n - 1) as u32) } else { 0 })
            .sum::<u32>()
            .to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let original_cards: Vec<_> = input
            .lines()
            .map(str::trim)
            .filter(|l| !l.is_empty())
            .map(ScratchCard::try_from)
            .map(Result::unwrap)
            .collect();

        let copy_ranges: HashMap<u32, RangeInclusive<_>> =
            HashMap::from_iter(original_cards.iter().map(|card| {
                let matches = card.count_matching_numbers() as u32;
                (card.id, ((card.id + 1)..=(card.id + matches)))
            }));

        let mut counts: HashMap<u32, usize> =
            HashMap::from_iter(original_cards.iter().map(|card| (card.id, 0)));

        original_cards.iter().for_each(|card| {
            let self_count = counts.get_mut(&card.id).unwrap();
            *self_count += 1;
            let self_count = *self_count;

            let rng = copy_ranges.get(&card.id).unwrap();
            rng.clone().for_each(|copy_id| {
                *counts.get_mut(&copy_id).unwrap() += self_count;
            })
        });

        counts.values().sum::<usize>().to_string()
    }
}

struct ScratchCard {
    id: u32,
    winning_numbers: HashSet<u32>,
    your_numbers: HashSet<u32>,
}

impl ScratchCard {
    fn count_matching_numbers(&self) -> usize {
        self.winning_numbers
            .intersection(&self.your_numbers)
            .count()
    }
}

fn parse_numbers(numbers_raw: &str) -> Result<Vec<u32>, &'static str> {
    numbers_raw
        .split_whitespace()
        .filter_map(|raw| {
            if raw.trim().is_empty() {
                return None;
            }
            Some(
                raw.trim()
                    .parse::<u32>()
                    .map_err(|_| "Cannot parse winning number!"),
            )
        })
        .collect::<Result<Vec<_>, _>>()
}

impl TryFrom<&str> for ScratchCard {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let (card, numbers) = value.trim().split_once(':').ok_or("Expected a colon!")?;
        let id = card
            .strip_prefix("Card ")
            .ok_or("Expected 'Card ' prefix!")?
            .trim()
            .parse()
            .map_err(|_| "Cannot parse card ID!")?;

        let (winning_numbers_raw, your_numbers_raw) =
            numbers.split_once('|').ok_or("Pipe symbol expected!")?;

        let winning_numbers = HashSet::from_iter(parse_numbers(winning_numbers_raw)?);
        let your_numbers = HashSet::from_iter(parse_numbers(your_numbers_raw)?);

        Ok(ScratchCard {
            id,
            winning_numbers,
            your_numbers,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
      Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
      Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
      Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
      Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
      Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
      Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
    ";

    #[test]
    fn test_day4_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "13");
    }

    #[test]
    fn test_day4_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "30");
    }
}
