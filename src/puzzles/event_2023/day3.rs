use itertools::Itertools;
use std::{
    collections::HashMap,
    ops::{RangeFrom, RangeInclusive},
};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let schematic: Schematic = input.try_into().unwrap();
        let numbers: Vec<_> = schematic
            .get_symbols()
            .iter()
            .flat_map(|(coord, _)| coord.get_neighbours())
            .filter_map(|coord| schematic.get_number(&coord))
            .unique()
            .map(|(_, _, n)| n)
            .collect();

        let sum = numbers.iter().sum::<u32>();
        sum.to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let schematic: Schematic = input.try_into().unwrap();
        let gears: Vec<_> = schematic
            .get_symbols()
            .iter()
            .filter(|(_, ch)| *ch == '*')
            .map(|(coord, _)| {
                coord
                    .get_neighbours()
                    .iter()
                    .filter_map(|coord| schematic.get_number(coord))
                    .unique()
                    .map(|(_, _, n)| n)
                    .collect::<Vec<_>>()
            })
            .filter(|numbers| numbers.len() == 2)
            .map(|numbers| numbers.iter().product::<u32>())
            .collect();

        gears.iter().sum::<u32>().to_string()
    }
}

#[derive(Debug, PartialEq, Clone, Eq, Hash)]
struct Coord {
    x: usize,
    y: usize,
}

impl Coord {
    fn get_neighbours(&self) -> Vec<Coord> {
        let x_range = (self.x.saturating_sub(1))..=(self.x + 1);
        let y_range = (self.y.saturating_sub(1))..=(self.y + 1);

        x_range
            .into_iter()
            .cartesian_product(y_range)
            .filter_map(|(x, y)| {
                let coord = Coord { x, y };
                if coord == *self {
                    return None;
                }
                Some(coord)
            })
            .collect()
    }
}

#[derive(Debug)]
struct Schematic {
    lines: Vec<SchematicLine>,
}

#[derive(Debug)]
struct SchematicLine {
    numbers: HashMap<RangeInclusive<usize>, u32>,
    symbols: HashMap<usize, char>,
}

#[derive(Default)]
struct ScanState {
    numbers: Vec<RangeInclusive<usize>>,
    number_started: Option<RangeFrom<usize>>,
    symbols: HashMap<usize, char>,
}

fn process_character(state: &mut ScanState, item: (usize, char)) -> Option<()> {
    let (x_idx, ch) = item;

    if ch.is_ascii_digit() {
        if state.number_started.is_none() {
            state.number_started = Some(RangeFrom { start: x_idx })
        }
    } else {
        if let Some(range) = &state.number_started {
            state
                .numbers
                .push(RangeInclusive::new(range.start, x_idx - 1));
            state.number_started = None;
        }

        if ch != '.' {
            state.symbols.insert(x_idx, ch);
        }
    }

    Some(())
}

impl Schematic {
    fn get_symbols(&self) -> Vec<(Coord, char)> {
        self.lines
            .iter()
            .enumerate()
            .flat_map(|(y, line)| {
                line.symbols
                    .iter()
                    .map(|(x, ch)| (Coord { x: *x, y }, *ch))
                    .collect::<Vec<_>>()
                    .into_iter()
            })
            .collect()
    }

    fn get_number(&self, coord: &Coord) -> Option<(usize, RangeInclusive<usize>, u32)> {
        if coord.y >= self.lines.len() {
            return None;
        }

        let line = self.lines.get(coord.y)?;

        line.numbers
            .keys()
            .find(|n| n.contains(&coord.x))
            .map(|key| (coord.y, key.clone(), *line.numbers.get(key).unwrap()))
    }
}

impl TryFrom<&str> for Schematic {
    type Error = &'static str;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        let lines = value.trim().lines().map(str::trim);

        let schematic_lines = lines
            .map(|line| {
                let mut state = ScanState::default();
                line.chars()
                    .enumerate()
                    .scan(&mut state, |state, item| process_character(state, item))
                    .for_each(|_| {});

                if let Some(range) = &state.number_started {
                    state
                        .numbers
                        .push(RangeInclusive::new(range.start, line.len() - 1))
                }

                let numbers = HashMap::from_iter(
                    state
                        .numbers
                        .into_iter()
                        .map(|range| (range.clone(), line[range].parse().unwrap())),
                );

                SchematicLine {
                    numbers,
                    symbols: state.symbols,
                }
            })
            .collect::<Vec<_>>();

        Ok(Schematic {
            lines: schematic_lines,
        })
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        467..114..
        ...*......
        ..35..633.
        ......#...
        617*......
        .....+.58.
        ..592.....
        ......755.
        ...$.*....
        .664.598..
    ";

    #[test]
    fn test_day3_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "4361");
    }

    #[test]
    fn test_day3_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "467835");
    }
}
