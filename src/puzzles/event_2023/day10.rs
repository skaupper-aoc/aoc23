use std::{
    clone::Clone,
    collections::{HashMap, HashSet, VecDeque},
    str::FromStr,
};

use itertools::Itertools;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let map = Map::from_str(input).unwrap();
        let main_loop = map.get_main_loop();
        main_loop.values().max().unwrap().to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let map = Map::from_str(input).unwrap();
        let enclosed_tiles = map.get_enclosed_tiles();
        enclosed_tiles.len().to_string()
    }
}

struct Map {
    tiles: HashMap<Point, Tile>,
    start_node: Point,
    width: usize,
    height: usize,
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let tiles: HashMap<_, _> =
            HashMap::from_iter(s.trim().lines().map(str::trim).enumerate().flat_map(
                |(line_idx, line)| {
                    line.chars().enumerate().map(move |(ch_idx, ch)| {
                        (
                            Point {
                                x: ch_idx,
                                y: line_idx,
                            },
                            Tile::try_from(ch).unwrap(),
                        )
                    })
                },
            ));

        let width = tiles
            .keys()
            .map(|p| p.x)
            .max_by(|x1, x2| x1.cmp(x2))
            .ok_or("Cannot find map width")?
            + 1;
        let height = tiles
            .keys()
            .map(|p| p.y)
            .max_by(|y1, y2| y1.cmp(y2))
            .ok_or("Cannot find map height")?
            + 1;
        let (start_node, _) = tiles
            .iter()
            .find(|(_, tile)| matches!(tile, Tile::Pipe(PipeDirection::Unknown)))
            .unwrap();
        let start_node = start_node.clone();

        let mut map = Map {
            tiles,
            start_node,
            width,
            height,
        };
        map.replace_unknown_pipe();
        Ok(map)
    }
}

impl Map {
    fn replace_unknown_pipe(&mut self) {
        let (unknown_node, _) = self
            .tiles
            .iter()
            .find(|(_, tile)| matches!(tile, Tile::Pipe(PipeDirection::Unknown)))
            .unwrap();

        let mut neighbours = get_connected_neighbours(self, unknown_node);
        let neigh = neighbours.next().unwrap();
        let dx1 = (neigh.x as i64) - (unknown_node.x as i64);
        let dy1 = (neigh.y as i64) - (unknown_node.y as i64);
        let neigh = neighbours.next().unwrap();
        let dx2 = (neigh.x as i64) - (unknown_node.x as i64);
        let dy2 = (neigh.y as i64) - (unknown_node.y as i64);
        drop(neighbours);

        let top = dy1 < 0 || dy2 < 0;
        let bottom = dy1 > 0 || dy2 > 0;
        let left = dx1 < 0 || dx2 < 0;
        let right = dx1 > 0 || dx2 > 0;

        let tile = Tile::Pipe(if top && left {
            PipeDirection::TopLeft
        } else if top && right {
            PipeDirection::TopRight
        } else if bottom && left {
            PipeDirection::BottomLeft
        } else if bottom && right {
            PipeDirection::BottomRight
        } else if top && bottom {
            PipeDirection::Vertical
        } else if left && right {
            PipeDirection::Horizontal
        } else {
            panic!("Invalid pipe direction!");
        });

        self.tiles.insert(unknown_node.clone(), tile);
    }

    fn get_enclosed_tiles(&self) -> Vec<Point> {
        let main_loop = self.get_main_loop();
        let main_loop: HashSet<_> = main_loop.keys().collect();
        let is_enclosed = |point: &Point| {
            // https://en.wikipedia.org/wiki/Point_in_polygon#Ray_casting_algorithm

            // Count all horizontal pipes above and below the given point.
            // To avoid problems with corners, virtually move the point by half a unit to the right -> Remove vertical pipes and left-going corners.
            let above = (0..point.y)
                .map(|y| Point { x: point.x, y })
                .filter(|point| main_loop.contains(point))
                .filter(|point| {
                    !matches!(
                        self.tiles[point],
                        Tile::Pipe(
                            PipeDirection::Vertical
                                | PipeDirection::BottomLeft
                                | PipeDirection::TopLeft
                        )
                    )
                })
                .count();
            let below = ((point.y + 1)..self.height)
                .map(|y| Point { x: point.x, y })
                .filter(|point| main_loop.contains(point))
                .filter(|point| {
                    !matches!(
                        self.tiles[point],
                        Tile::Pipe(
                            PipeDirection::Vertical
                                | PipeDirection::BottomLeft
                                | PipeDirection::TopLeft
                        )
                    )
                })
                .count();

            // Count all vertical pipes left and right of the given point.
            // To avoid problems with corners, virtually move the point by half a unit up -> Remove horizontal pipes and bottom-going corners.
            let left = (0..point.x)
                .map(|x| Point { x, y: point.y })
                .filter(|point| main_loop.contains(point))
                .filter(|point| {
                    !matches!(
                        self.tiles[point],
                        Tile::Pipe(
                            PipeDirection::Horizontal
                                | PipeDirection::BottomLeft
                                | PipeDirection::BottomRight
                        )
                    )
                })
                .count();
            let right = ((point.x + 1)..self.width)
                .map(|x| Point { x, y: point.y })
                .filter(|point| main_loop.contains(point))
                .filter(|point| {
                    !matches!(
                        self.tiles[point],
                        Tile::Pipe(
                            PipeDirection::Horizontal
                                | PipeDirection::BottomLeft
                                | PipeDirection::BottomRight
                        )
                    )
                })
                .count();

            (above % 2 != 0) || (below % 2 != 0) || (left % 2 != 0) || (right % 2 != 0)
        };

        self.tiles
            .iter()
            .filter_map(|(point, _)| {
                if main_loop.contains(point) {
                    None
                } else {
                    Some(point)
                }
            })
            .filter(|point| is_enclosed(point))
            .cloned()
            .collect()
    }

    fn get_main_loop(&self) -> HashMap<Point, usize> {
        let mut visited = HashMap::new();
        let mut to_visit = VecDeque::new();
        to_visit.push_back((self.start_node.clone(), 0));

        while !to_visit.is_empty() {
            let (point, depth) = to_visit.pop_front().unwrap();
            get_connected_neighbours(self, &point)
                .filter(|point| !visited.contains_key(point))
                .for_each(|point| to_visit.push_back((point.clone(), depth + 1)));
            visited.insert(point, depth);
        }

        visited
    }
}

#[derive(Debug, Eq, PartialEq, Hash, Clone)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(Clone, Copy)]
enum PipeDirection {
    Unknown,
    Vertical,
    Horizontal,
    BottomRight,
    BottomLeft,
    TopRight,
    TopLeft,
}

impl TryFrom<char> for PipeDirection {
    type Error = &'static str;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            'S' => Ok(PipeDirection::Unknown),
            '|' => Ok(PipeDirection::Vertical),
            '-' => Ok(PipeDirection::Horizontal),
            'F' => Ok(PipeDirection::BottomRight),
            '7' => Ok(PipeDirection::BottomLeft),
            'L' => Ok(PipeDirection::TopRight),
            'J' => Ok(PipeDirection::TopLeft),
            _ => Err("Unknown pipe direction!"),
        }
    }
}

#[derive(Clone, Copy)]
enum Tile {
    Empty,
    Pipe(PipeDirection),
}

impl TryFrom<char> for Tile {
    type Error = &'static str;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        match value {
            '.' => Ok(Tile::Empty),
            _ => Ok(Tile::Pipe(PipeDirection::try_from(value)?)),
        }
    }
}

fn get_connected_neighbours<'a>(map: &'a Map, node: &'a Point) -> impl Iterator<Item = Point> + 'a {
    let x = (node.x.saturating_sub(1))..=(node.x + 1);
    let y = (node.y.saturating_sub(1))..=(node.y + 1);

    x.cartesian_product(y)
        .map(|(x, y)| Point { x, y })
        .filter(move |point| !(point == node || point.x >= map.width || point.y >= map.height))
        .filter(move |point| {
            let node_tile = map.tiles[node];
            let point_tile = map.tiles[point];

            let dx = (point.x as i64) - (node.x as i64);
            let dy = (point.y as i64) - (node.y as i64);
            if dx.abs() + dy.abs() != 1 {
                return false;
            }

            let point_maybe_connected = match point_tile {
                Tile::Empty => false,
                Tile::Pipe(PipeDirection::Unknown) => true,
                Tile::Pipe(PipeDirection::Vertical) => dx == 0,
                Tile::Pipe(PipeDirection::Horizontal) => dy == 0,
                Tile::Pipe(PipeDirection::BottomRight) => dy < 0 || dx < 0,
                Tile::Pipe(PipeDirection::BottomLeft) => dy < 0 || dx > 0,
                Tile::Pipe(PipeDirection::TopRight) => dy > 0 || dx < 0,
                Tile::Pipe(PipeDirection::TopLeft) => dy > 0 || dx > 0,
            };

            let node_maybe_connected = match node_tile {
                Tile::Empty => false,
                Tile::Pipe(PipeDirection::Unknown) => true,
                Tile::Pipe(PipeDirection::Vertical) => dx == 0,
                Tile::Pipe(PipeDirection::Horizontal) => dy == 0,
                Tile::Pipe(PipeDirection::BottomRight) => dy > 0 || dx > 0,
                Tile::Pipe(PipeDirection::BottomLeft) => dy > 0 || dx < 0,
                Tile::Pipe(PipeDirection::TopRight) => dy < 0 || dx > 0,
                Tile::Pipe(PipeDirection::TopLeft) => dy < 0 || dx < 0,
            };
            point_maybe_connected && node_maybe_connected
        })
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        .....
        .S-7.
        .|.|.
        .L-J.
        .....
    ";

    const EXAMPLE_2: &str = "
        ..F7.
        .FJ|.
        SJ.L7
        |F--J
        LJ...
    ";

    const EXAMPLE_3: &str = "
        ...........
        .S-------7.
        .|F-----7|.
        .||.....||.
        .||.....||.
        .|L-7.F-J|.
        .|..|.|..|.
        .L--J.L--J.
        ...........
    ";

    const EXAMPLE_4: &str = "
        ..........
        .S------7.
        .|F----7|.
        .||....||.
        .||....||.
        .|L-7F-J|.
        .|..||..|.
        .L--JL--J.
        ..........
    ";

    const EXAMPLE_5: &str = "
        .F----7F7F7F7F-7....
        .|F--7||||||||FJ....
        .||.FJ||||||||L7....
        FJL7L7LJLJ||LJ.L-7..
        L--J.L7...LJS7F-7L7.
        ....F-J..F7FJ|L7L7L7
        ....L7.F7||L7|.L7L7|
        .....|FJLJ|FJ|F7|.LJ
        ....FJL-7.||.||||...
        ....L---J.LJ.LJLJ...
    ";

    const EXAMPLE_6: &str = "
        FF7FSF7F7F7F7F7F---7
        L|LJ||||||||||||F--J
        FL-7LJLJ||||||LJL-77
        F--JF--7||LJLJ7F7FJ-
        L---JF-JLJ.||-FJLJJ7
        |F|F-JF---7F7-L7L|7|
        |FFJF7L7F-JF7|JL---7
        7-L-JL7||F7|L7F-7F7|
        L.L7LFJ|||||FJL7||LJ
        L7JLJL-JLJLJL--JLJ.L
    ";

    #[test]
    fn test_day10_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "4");
    }

    #[test]
    fn test_day10_part1_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_2), "8");
    }

    #[test]
    fn test_day10_part2_example3() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_3), "4");
    }

    #[test]
    fn test_day10_part2_example4() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_4), "4");
    }

    #[test]
    fn test_day10_part2_example5() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_5), "8");
    }

    #[test]
    fn test_day10_part2_example6() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_6), "10");
    }
}
