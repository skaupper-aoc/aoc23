use std::{
    collections::{HashMap, HashSet},
    str::FromStr,
};

use itertools::Itertools;
use num::Signed;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation {
    steps: u32,
}

impl DayTrait for Implementation {
    fn init_part1(&mut self) {
        self.steps = 64;
    }

    fn call_part1(&self, input: &str) -> String {
        let map = Map::from_str(input.trim()).unwrap();
        let points = map.get_points_after_steps(self.steps, false);
        points.len().to_string()
    }

    fn init_part2(&mut self) {
        self.steps = 26501365;
    }

    fn call_part2(&self, input: &str) -> String {
        let map = Map::from_str(input.trim()).unwrap();
        let points = map.get_points_after_steps(self.steps, true);
        points.len().to_string()
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
enum Tile {
    Garden,
    Rock,
    Start,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Point {
    x: i64,
    y: i64,
}

#[derive(PartialEq, Eq, Clone)]
struct Map {
    map: HashMap<Point, Tile>,
    start_position: Point,
    width: usize,
    height: usize,
}

impl TryFrom<char> for Tile {
    type Error = &'static str;

    fn try_from(value: char) -> Result<Self, Self::Error> {
        Ok(match value {
            '.' => Tile::Garden,
            '#' => Tile::Rock,
            'S' => Tile::Start,
            _ => Err("Cannot parse tile")?,
        })
    }
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let map = HashMap::from_iter(s.trim().lines().enumerate().flat_map(|(y, line)| {
            line.trim().chars().enumerate().map(move |(x, ch)| {
                (
                    Point {
                        x: x as i64,
                        y: y as i64,
                    },
                    Tile::try_from(ch).unwrap(),
                )
            })
        }));

        let (start_position, _) = map
            .iter()
            .find(|(_, tile)| **tile == Tile::Start)
            .ok_or("Cannot find start position")?;
        let start_position = *start_position;
        let height = (map.keys().map(|p| p.y).max().unwrap() + 1) as usize;
        let width = (map.keys().map(|p| p.x).max().unwrap() + 1) as usize;

        Ok(Map {
            map,
            start_position,
            width,
            height,
        })
    }
}

impl Point {
    fn get_neighbours(&self, map: &Map, repeat_map: bool) -> Vec<Point> {
        (-1..=1)
            .cartesian_product(-1..=1)
            .filter(|(dx, dy)| dx.abs() + dy.abs() == 1)
            .map(|(dx, dy)| ((self.x as i64) + dx, (self.y as i64) + dy))
            .filter(|(x, y)| {
                repeat_map
                    || ((*x >= 0 && *x < (map.width as i64))
                        && (*y >= 0 && *y < (map.height as i64)))
            })
            .map(|(x, y)| Point { x, y })
            .filter(|point| {
                let point_in_map = Point {
                    x: point.x.rem_euclid(map.width as i64),
                    y: point.y.rem_euclid(map.height as i64),
                };
                map.map[&point_in_map] != Tile::Rock
            })
            .collect()
    }
}

impl Map {
    fn _do_step(&self, start_points: &HashSet<Point>, repeat_map: bool) -> HashSet<Point> {
        HashSet::from_iter(
            start_points
                .iter()
                .flat_map(|point| point.get_neighbours(self, repeat_map)),
        )
    }

    fn get_points_after_steps(&self, steps: u32, repeat_map: bool) -> HashSet<Point> {
        (0..steps).fold(
            HashSet::from_iter(vec![self.start_position]),
            |points, _| self._do_step(&points, repeat_map),
        )
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = r"
        ...........
        .....###.#.
        .###.##..#.
        ..#.#...#..
        ....#.#....
        .##..S####.
        .##..#...#.
        .......##..
        .##.#.####.
        .##..##.##.
        ...........
    ";

    #[test]
    fn test_day21_part1_example1() {
        let implementation = Implementation { steps: 6 };
        assert_eq!(implementation.call_part1(EXAMPLE_1), "16");
    }

    #[test]
    fn test_day21_part2_example1() {
        let implementation = Implementation { steps: 6 };
        assert_eq!(implementation.call_part2(EXAMPLE_1), "16");
    }

    #[test]
    fn test_day21_part2_example2() {
        let implementation = Implementation { steps: 10 };
        assert_eq!(implementation.call_part2(EXAMPLE_1), "50");
    }

    #[test]
    fn test_day21_part2_example3() {
        let implementation = Implementation { steps: 50 };
        assert_eq!(implementation.call_part2(EXAMPLE_1), "1594");
    }

    #[test]
    fn test_day21_part2_example4() {
        let implementation = Implementation { steps: 100 };
        assert_eq!(implementation.call_part2(EXAMPLE_1), "6536");
    }

    // #[test]
    // fn test_day21_part2_example5() {
    //     let implementation = Implementation { steps: 500 };
    //     assert_eq!(implementation.call_part2(EXAMPLE_1), "167004");
    // }

    // #[test]
    // fn test_day21_part2_example6() {
    //     let implementation = Implementation { steps: 1000 };
    //     assert_eq!(implementation.call_part2(EXAMPLE_1), "668697");
    // }

    // #[test]
    // fn test_day21_part2_example7() {
    //     let implementation = Implementation { steps: 5000 };
    //     assert_eq!(implementation.call_part2(EXAMPLE_1), "16733044");
    // }
}
