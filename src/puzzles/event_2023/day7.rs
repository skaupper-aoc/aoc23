use std::str::FromStr;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let mut hands: Vec<_> = input
            .trim()
            .lines()
            .map(str::trim)
            .map(part1::Hand::from_str)
            .map(Result::unwrap)
            .collect();

        hands.sort();

        let sum = hands
            .iter()
            .enumerate()
            .map(|(idx, hand)| (idx + 1) * (hand.bid as usize))
            .sum::<usize>();

        sum.to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let mut hands: Vec<_> = input
            .trim()
            .lines()
            .map(str::trim)
            .map(part2::Hand::from_str)
            .map(Result::unwrap)
            .collect();

        hands.sort();

        let sum = hands
            .iter()
            .enumerate()
            .map(|(idx, hand)| (idx + 1) * (hand.bid as usize))
            .sum::<usize>();

        sum.to_string()
    }
}

mod part1 {
    use std::str::FromStr;

    use itertools::Itertools;

    #[derive(PartialEq, PartialOrd, Debug, Hash, Eq, Ord)]
    pub(super) enum Card {
        Ace = 14,
        King = 13,
        Queen = 12,
        Jack = 11,
        Ten = 10,
        Nine = 9,
        Eight = 8,
        Seven = 7,
        Six = 6,
        Five = 5,
        Four = 4,
        Three = 3,
        Two = 2,
    }

    #[derive(PartialEq, Eq)]
    pub(super) struct Hand {
        pub(super) cards: [Card; 5],
        pub(super) bid: u32,
    }

    #[derive(PartialEq, PartialOrd, Ord, Eq)]
    pub(super) enum HandType {
        FiveOfAKind = 7,
        FourOfAKind = 6,
        FullHouse = 5,
        ThreeOfAKind = 4,
        TwoPair = 3,
        OnePair = 2,
        HighCard = 1,
    }

    impl FromStr for Card {
        type Err = &'static str;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            if s.len() != 1 {
                return Err("Cards can only be created from single character strings!");
            }

            Ok(match s {
                "A" | "a" => Card::Ace,
                "K" | "k" => Card::King,
                "Q" | "q" => Card::Queen,
                "J" | "j" => Card::Jack,
                "T" | "t" => Card::Ten,
                "9" => Card::Nine,
                "8" => Card::Eight,
                "7" => Card::Seven,
                "6" => Card::Six,
                "5" => Card::Five,
                "4" => Card::Four,
                "3" => Card::Three,
                "2" => Card::Two,
                _ => return Err("Unknown card!"),
            })
        }
    }

    impl FromStr for Hand {
        type Err = &'static str;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let (cards, bid) = s.split_once(' ').ok_or("Invalid hand format")?;

            let cards: [Card; 5] = cards
                .char_indices()
                .map(|(pos, ch)| pos..(pos + ch.len_utf8()))
                .map(|rng| s.get(rng).unwrap())
                .map(Card::from_str)
                .map(Result::unwrap)
                .collect::<Vec<_>>()
                .try_into()
                .map_err(|_| "Cannot collect cards into array")?;

            let bid = bid.parse::<u32>().map_err(|_| "Cannot parse bid")?;

            Ok(Hand { cards, bid })
        }
    }

    impl PartialOrd for Hand {
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Ord for Hand {
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            let self_type = self.get_type();
            let other_type = other.get_type();
            if self_type != other_type {
                return self_type.cmp(&other_type);
            }

            self.cards.cmp(&other.cards)
        }
    }

    impl Hand {
        fn get_type(&self) -> HandType {
            let counts = self.cards.iter().counts();

            let mut counts: Vec<_> = counts.values().collect();
            counts.sort();
            counts.reverse();

            let first_highest = **counts.get(0).unwrap();
            let second_highest = **counts.get(1).unwrap_or(&&0);

            if first_highest == 5 {
                HandType::FiveOfAKind
            } else if first_highest == 4 {
                HandType::FourOfAKind
            } else if first_highest + second_highest == 5 {
                HandType::FullHouse
            } else if first_highest == 3 {
                HandType::ThreeOfAKind
            } else if first_highest + second_highest == 4 {
                HandType::TwoPair
            } else if first_highest == 2 {
                HandType::OnePair
            } else {
                HandType::HighCard
            }
        }
    }
}

mod part2 {
    use std::str::FromStr;

    use itertools::Itertools;

    #[derive(PartialEq, PartialOrd, Debug, Hash, Eq, Ord)]
    pub(super) enum Card {
        Ace = 14,
        King = 13,
        Queen = 12,
        Ten = 10,
        Nine = 9,
        Eight = 8,
        Seven = 7,
        Six = 6,
        Five = 5,
        Four = 4,
        Three = 3,
        Two = 2,
        Joker = 1,
    }

    #[derive(PartialEq, Eq, Debug)]
    pub(super) struct Hand {
        pub(super) cards: [Card; 5],
        pub(super) bid: u32,
    }

    #[derive(PartialEq, PartialOrd, Debug, Ord, Eq)]
    pub(super) enum HandType {
        FiveOfAKind = 7,
        FourOfAKind = 6,
        FullHouse = 5,
        ThreeOfAKind = 4,
        TwoPair = 3,
        OnePair = 2,
        HighCard = 1,
    }

    impl FromStr for Card {
        type Err = &'static str;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            if s.len() != 1 {
                return Err("Cards can only be created from single character strings!");
            }

            Ok(match s {
                "A" => Card::Ace,
                "K" => Card::King,
                "Q" => Card::Queen,
                "J" => Card::Joker,
                "T" => Card::Ten,
                "9" => Card::Nine,
                "8" => Card::Eight,
                "7" => Card::Seven,
                "6" => Card::Six,
                "5" => Card::Five,
                "4" => Card::Four,
                "3" => Card::Three,
                "2" => Card::Two,
                _ => return Err("Unknown card!"),
            })
        }
    }

    impl FromStr for Hand {
        type Err = &'static str;

        fn from_str(s: &str) -> Result<Self, Self::Err> {
            let (cards, bid) = s.split_once(' ').ok_or("Invalid hand format")?;

            let cards: [Card; 5] = cards
                .trim()
                .char_indices()
                .map(|(pos, ch)| pos..(pos + ch.len_utf8()))
                .map(|rng| s.get(rng).unwrap())
                .map(Card::from_str)
                .map(Result::unwrap)
                .collect::<Vec<_>>()
                .try_into()
                .map_err(|_| "Cannot collect cards into array")?;

            let bid = bid.trim().parse::<u32>().map_err(|_| "Cannot parse bid")?;

            Ok(Hand { cards, bid })
        }
    }

    impl PartialOrd for Hand {
        fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
            Some(self.cmp(other))
        }
    }

    impl Ord for Hand {
        fn cmp(&self, other: &Self) -> std::cmp::Ordering {
            let self_type = self.get_type();
            let other_type = other.get_type();
            if self_type != other_type {
                return self_type.cmp(&other_type);
            }

            self.cards.cmp(&other.cards)
        }
    }

    impl Hand {
        fn get_type(&self) -> HandType {
            let counts = self.cards.iter().counts();

            let joker_cnt = *counts.get(&Card::Joker).unwrap_or(&0);
            let mut counts: Vec<_> = counts
                .iter()
                .filter(|(card, _)| ***card != Card::Joker)
                .map(|(_, count)| count)
                .collect();

            counts.sort();
            counts.reverse();

            let first_highest = **counts.get(0).unwrap_or(&&0);
            let second_highest = **counts.get(1).unwrap_or(&&0);

            if first_highest + joker_cnt == 5 {
                HandType::FiveOfAKind
            } else if first_highest + joker_cnt == 4 {
                HandType::FourOfAKind
            } else if first_highest + second_highest + joker_cnt == 5 {
                HandType::FullHouse
            } else if first_highest + joker_cnt == 3 {
                HandType::ThreeOfAKind
            } else if first_highest + second_highest + joker_cnt == 4 {
                HandType::TwoPair
            } else if first_highest + joker_cnt == 2 {
                HandType::OnePair
            } else {
                HandType::HighCard
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        32T3K 765
        T55J5 684
        KK677 28
        KTJJT 220
        QQQJA 483
    ";

    const EXAMPLE_2: &str = "
        JKKK2 1
        QQQQ2 10
    ";

    #[test]
    fn test_day7_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "6440");
    }

    #[test]
    fn test_day7_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "5905");
    }

    #[test]
    fn test_day7_part2_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_2), "21");
    }
}
