use itertools::Itertools;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let numbers: (Vec<u32>, Vec<u32>) = input
            .trim()
            .lines()
            .map(str::trim)
            .map(|l| {
                l.split_whitespace()
                    .skip(1)
                    .map(|n| n.parse::<u32>().unwrap())
                    .collect()
            })
            .next_tuple()
            .unwrap();

        let races = numbers.0.into_iter().zip(numbers.1);

        let possibilities = races
            .map(|(duration, record)| {
                let a = -1;
                let b = duration as i64;
                let c = -(record as i64);
                quadratic_formula(a, b, c)
            })
            .map(|(res_low, res_high)| {
                (
                    (res_low + 0.0001f64).ceil() as u32,
                    (res_high - 0.0001f64).floor() as u32,
                )
            })
            .map(|(res_low, res_high)| res_high - res_low + 1)
            .collect_vec();

        possibilities.iter().product::<u32>().to_string()
    }

    fn call_part2(&self, input: &str) -> String {
        let numbers: (Vec<u64>, Vec<u64>) = input
            .trim()
            .lines()
            .map(str::trim)
            .map(|l| {
                l.replace(' ', "")
                    .split(':')
                    .skip(1)
                    .map(|n| n.parse::<u64>().unwrap())
                    .collect()
            })
            .next_tuple()
            .unwrap();

        let races = numbers.0.into_iter().zip(numbers.1);

        let possibilities = races
            .map(|(duration, record)| {
                let a = -1;
                let b = duration as i64;
                let c = -(record as i64);
                quadratic_formula(a, b, c)
            })
            .map(|(res_low, res_high)| (res_low.ceil() as u64, res_high.floor() as u64))
            .map(|(res_low, res_high)| res_high - res_low + 1)
            .collect_vec();

        possibilities.iter().product::<u64>().to_string()
    }
}

fn quadratic_formula(a: i64, b: i64, c: i64) -> (f64, f64) {
    let root = ((b * b - 4 * a * c) as f64).sqrt();
    let res1 = (-(b as f64) + root) / 2f64 * (a as f64);
    let res2 = (-(b as f64) - root) / 2f64 * (a as f64);
    (res1, res2)
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        Time:      7  15   30
        Distance:  9  40  200
    ";

    #[test]
    fn test_day6_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "288");
    }

    #[test]
    fn test_day6_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "71503");
    }
}
