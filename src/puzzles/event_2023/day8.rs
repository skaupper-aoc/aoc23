use std::collections::HashMap;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let mut lines = input.trim().lines().map(str::trim);
        let directions: Vec<_> = lines
            .next()
            .unwrap()
            .chars()
            .map(|c| match c {
                'R' => Direction::Right,
                'L' => Direction::Left,
                _ => panic!("Cannot parse direction"),
            })
            .collect();

        let raw_nodes: Vec<_> = lines.skip(1).map(RawNode::from_str).collect();
        let nodes_map: HashMap<&str, RawNode<'_>> =
            HashMap::from_iter(raw_nodes.into_iter().map(|node| (node.name, node)));

        steps_to_end("AAA", &directions, &nodes_map).to_string()
    }

    fn call_part2(&self, input: &str) -> String {
        let mut lines = input.trim().lines().map(str::trim);
        let directions: Vec<_> = lines
            .next()
            .unwrap()
            .chars()
            .map(|c| match c {
                'R' => Direction::Right,
                'L' => Direction::Left,
                _ => panic!("Cannot parse direction"),
            })
            .collect();

        let raw_nodes: Vec<_> = lines.skip(1).map(RawNode::from_str).collect();
        let nodes_map: HashMap<&str, RawNode<'_>> =
            HashMap::from_iter(raw_nodes.into_iter().map(|node| (node.name, node)));

        let common_period = nodes_map
            .keys()
            .filter(|node| node.ends_with('A'))
            .map(|start_node| steps_to_end(start_node, &directions, &nodes_map) as u64)
            .fold(1, num::integer::lcm);

        common_period.to_string()
    }
}

fn steps_to_end<'a>(
    current: &'a str,
    directions: &[Direction],
    nodes_map: &HashMap<&'a str, RawNode<'a>>,
) -> u32 {
    let mut current = current;
    let mut steps = 0;
    let _ = directions
        .iter()
        .cycle()
        .scan(&mut current, |state, dir| {
            let curr = nodes_map.get(*state).unwrap();
            match dir {
                Direction::Left => **state = nodes_map.get(&curr.left).unwrap().name,
                Direction::Right => **state = nodes_map.get(&curr.right).unwrap().name,
            }

            steps += 1;
            if state.ends_with('Z') {
                None
            } else {
                Some(**state)
            }
        })
        .last()
        .unwrap();

    steps
}

enum Direction {
    Left,
    Right,
}

#[derive(Debug)]
struct RawNode<'a> {
    name: &'a str,
    left: &'a str,
    right: &'a str,
}
impl<'a> RawNode<'a> {
    fn from_str(s: &'a str) -> Self {
        let (name, children) = s.split_once('=').ok_or("Cannot parse node").unwrap();
        let (left, right) = children
            .split_once(',')
            .ok_or("Cannot parse child nodes")
            .unwrap();

        RawNode {
            name: name.trim(),
            left: left.trim().strip_prefix('(').unwrap().trim(),
            right: right.trim().strip_suffix(')').unwrap().trim(),
        }
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        RL

        AAA = (BBB, CCC)
        BBB = (DDD, EEE)
        CCC = (ZZZ, GGG)
        DDD = (DDD, DDD)
        EEE = (EEE, EEE)
        GGG = (GGG, GGG)
        ZZZ = (ZZZ, ZZZ)
    ";

    const EXAMPLE_2: &str = "
        LLR

        AAA = (BBB, BBB)
        BBB = (AAA, ZZZ)
        ZZZ = (ZZZ, ZZZ)
    ";

    const EXAMPLE_3: &str = "
        LR

        11A = (11B, XXX)
        11B = (XXX, 11Z)
        11Z = (11B, XXX)
        22A = (22B, XXX)
        22B = (22C, 22C)
        22C = (22Z, 22Z)
        22Z = (22B, 22B)
        XXX = (XXX, XXX)
    ";

    #[test]
    fn test_day8_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "2");
    }

    #[test]
    fn test_day8_part1_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_2), "6");
    }

    #[test]
    fn test_day8_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "2");
    }

    #[test]
    fn test_day8_part2_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_2), "6");
    }

    #[test]
    fn test_day8_part2_example3() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_3), "6");
    }
}
