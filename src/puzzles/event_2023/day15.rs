use std::{collections::LinkedList, str::FromStr};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let sum = input
            .trim()
            .split(',')
            .map(hash)
            .map(|val| val as u32)
            .sum::<u32>();
        sum.to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let mut boxes = Vec::from_iter((0..256).map(|_| Box::default()));

        input
            .trim()
            .split(',')
            .map(Instruction::from_str)
            .map(Result::unwrap)
            .for_each(|instr| instr.apply(&mut boxes));

        let focusing_power = boxes
            .iter()
            .enumerate()
            .filter(|(_, r#box)| !r#box.lenses.is_empty())
            .map(|(box_idx, r#box)| {
                (box_idx + 1)
                    * r#box
                        .lenses
                        .iter()
                        .enumerate()
                        .map(|(lens_idx, lens)| (lens_idx + 1) * (lens.focal_length as usize))
                        .sum::<usize>()
            })
            .sum::<usize>();

        focusing_power.to_string()
    }
}

fn hash(s: &str) -> u8 {
    s.chars()
        .map(|ch| ch as u8)
        .fold(0, |curr, val| curr.wrapping_add(val).wrapping_mul(17))
}

#[derive(Debug)]
enum Operation {
    Remove,
    Add(u8),
}

#[derive(Debug)]
struct Instruction {
    label: String,
    operation: Operation,
}

#[derive(Debug, Clone)]
struct Lens {
    label: String,
    focal_length: u8,
}

#[derive(Debug, Default)]
struct Box {
    lenses: LinkedList<Lens>,
}

impl FromStr for Instruction {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let label = s
            .chars()
            .take_while(|ch| ch.is_alphabetic())
            .collect::<String>();
        let len = label.len();
        Ok(Instruction {
            label,
            operation: Operation::from_str(&s[len..])?,
        })
    }
}
impl FromStr for Operation {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.is_empty() || s.len() > 2 {
            println!("{s}");
            return Err("Operations need to be either one or two characters");
        }
        let op = s.get(0..1).unwrap();
        let lens = s.get(1..2);
        match op {
            "-" => Ok(Operation::Remove),
            "=" => {
                let lens = lens
                    .ok_or("Add operation must have a lens given too!")?
                    .parse()
                    .map_err(|_| "Cannot parse lens number!")?;
                Ok(Operation::Add(lens))
            }
            _ => Err("Unknown operation"),
        }
    }
}

impl Instruction {
    fn apply(&self, boxes: &mut [Box]) {
        let box_idx = hash(&self.label);
        match &self.operation {
            Operation::Remove => {
                boxes[box_idx as usize].remove(&self.label);
            }
            Operation::Add(lens) => {
                boxes[box_idx as usize].replace_or_push(&self.label, *lens);
            }
        }
    }
}
impl Box {
    fn remove(&mut self, label: &str) {
        self.lenses =
            LinkedList::from_iter(self.lenses.iter().filter(|l| l.label != label).cloned());
    }

    fn replace_or_push(&mut self, label: &str, focal_length: u8) {
        let old_lens = self.lenses.iter_mut().find(|lens| lens.label == label);
        if let Some(lens) = old_lens {
            lens.focal_length = focal_length;
        } else {
            self.lenses.push_back(Lens {
                label: label.to_owned(),
                focal_length,
            });
        }
    }
}
#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
    ";

    #[test]
    fn test_day15_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "1320");
    }

    #[test]
    fn test_day15_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "145");
    }
}
