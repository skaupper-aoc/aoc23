use std::collections::HashSet;

use itertools::Itertools;
use num::{integer::Roots, Integer};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let lines = input
            .trim()
            .lines()
            .map(str::trim)
            .map(|line| {
                let (dir, distance) = line.split_whitespace().take(2).next_tuple().unwrap();
                let distance = distance.parse::<u32>().unwrap();
                let dir_vec = match dir {
                    "R" => Vector { x: 1, y: 0 },
                    "L" => Vector { x: -1, y: 0 },
                    "D" => Vector { x: 0, y: 1 },
                    "U" => Vector { x: 0, y: -1 },
                    _ => panic!("Unknown direction!"),
                } * distance;

                let start = Point { x: 0, y: 0 };
                Line {
                    start,
                    end: start + dir_vec,
                }
            })
            .scan(Point { x: 0, y: 0 }, |point, line| {
                let line = line + Vector::from(*point);
                *point = line.end;
                Some(line)
            })
            .collect::<Vec<_>>();

        let polygon = Polygon { lines };
        polygon.plot(true);

        let points = polygon.get_points(true);
        points.len().to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        input.into()
    }
}
#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Point {
    x: i64,
    y: i64,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Vector {
    x: i64,
    y: i64,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Line {
    start: Point,
    end: Point,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Polygon {
    lines: Vec<Line>,
}

impl std::ops::Add<Vector> for Point {
    type Output = Point;

    fn add(self, rhs: Vector) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl std::ops::Sub<Point> for Point {
    type Output = Vector;

    fn sub(self, rhs: Point) -> Self::Output {
        Vector {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl From<Point> for Vector {
    fn from(value: Point) -> Self {
        Vector {
            x: value.x,
            y: value.y,
        }
    }
}

impl std::ops::Add<Vector> for Line {
    type Output = Line;

    fn add(self, rhs: Vector) -> Self::Output {
        Line {
            start: self.start + rhs,
            end: self.end + rhs,
        }
    }
}

impl<T: Into<i64>> std::ops::Mul<T> for Vector {
    type Output = Vector;

    fn mul(self, rhs: T) -> Self::Output {
        let factor = rhs.into();
        Vector {
            x: self.x * factor,
            y: self.y * factor,
        }
    }
}

impl Line {
    fn direction(&self) -> Vector {
        (self.end - self.start).normalize()
    }

    fn shift_and_scale(&self, factor: u32) -> Line {
        let factor = factor as i64;
        Line {
            start: Point {
                x: self.start.x * factor,
                y: self.start.y * factor,
            },
            end: Point {
                x: self.end.x * factor,
                y: self.end.y * factor,
            },
        }
    }

    fn intersect(&self, other: &Line) -> bool {
        let is_between = |val, coord1, coord2| {
            std::cmp::min(coord1, coord2) <= val && val <= std::cmp::max(coord1, coord2)
        };
        let self_start_x_between = is_between(self.start.x, other.start.x, other.end.x);
        let self_end_x_between = is_between(self.end.x, other.start.x, other.end.x);
        let other_start_x_between = is_between(other.start.x, self.start.x, self.end.x);
        let other_end_x_between = is_between(other.end.x, self.start.x, self.end.x);

        let self_start_y_between = is_between(self.start.y, other.start.y, other.end.y);
        let self_end_y_between = is_between(self.end.y, other.start.y, other.end.y);
        let other_start_y_between = is_between(other.start.y, self.start.y, self.end.y);
        let other_end_y_between = is_between(other.end.y, self.start.y, self.end.y);

        let vertical_intersection = (other_start_x_between && other_end_x_between)
            && (self_start_y_between && self_end_y_between);
        let horizontal_intersection = (self_start_x_between && self_end_x_between)
            && (other_start_y_between && other_end_y_between);

        vertical_intersection || horizontal_intersection
    }
}

impl Vector {
    fn length(&self) -> i64 {
        (self.x * self.x + self.y * self.y).sqrt()
    }

    fn normalize(self) -> Vector {
        if self.x != 0 && self.y != 0 {
            panic!("Cannot normalize a non-orthogonal vector!");
        }

        let length = self.length();
        Vector {
            x: self.x / length,
            y: self.y / length,
        }
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone)]
struct Rectangle {
    top_left: Point,
    bottom_right: Point,
}

impl Rectangle {
    fn iter(&self) -> impl Iterator<Item = Point> {
        let y_range = self.top_left.y..=self.bottom_right.y;
        let x_range = self.top_left.x..=self.bottom_right.x;

        y_range
            .cartesian_product(x_range)
            .map(|(y, x)| Point { x, y })
    }
}

impl Polygon {
    fn get_boundary_box(&self) -> Rectangle {
        let min_x = self
            .lines
            .iter()
            .flat_map(|line| vec![line.start.x, line.end.x])
            .min()
            .unwrap();
        let min_y = self
            .lines
            .iter()
            .flat_map(|line| vec![line.start.y, line.end.y])
            .min()
            .unwrap();
        let max_x = self
            .lines
            .iter()
            .flat_map(|line| vec![line.start.x, line.end.x])
            .max()
            .unwrap();
        let max_y = self
            .lines
            .iter()
            .flat_map(|line| vec![line.start.y, line.end.y])
            .max()
            .unwrap();

        Rectangle {
            top_left: Point { x: min_x, y: min_y },
            bottom_right: Point { x: max_x, y: max_y },
        }
    }

    fn get_points(&self, include_enclosed: bool) -> HashSet<Point> {
        let boundary_box = self.get_boundary_box();
        let boundary_points = self.lines.iter().flat_map(|line| {
            let vec = (line.end - line.start).normalize();
            std::iter::once(line.start).chain((0..).scan(line.start, move |point, _| {
                *point = *point + vec;
                if *point == line.end {
                    None
                } else {
                    Some(*point)
                }
            }))
        });

        if !include_enclosed {
            return boundary_points.collect();
        }

        let inner_points = boundary_box.iter().filter(|point| {
            let up_vec = Vector {
                x: 0,
                y: boundary_box.top_left.y - boundary_box.bottom_right.y,
            };
            let test_line = Line {
                start: *point,
                end: *point + up_vec,
            }
            .shift_and_scale(2)
                + Vector { x: -1, y: -1 };

            self.lines
                .iter()
                .map(|line| line.shift_and_scale(2))
                .filter(|line| {
                    let intersected = line.intersect(&test_line);
                    // if intersected {
                    //     print!("Y: ");
                    // } else {
                    //     print!("N: ");
                    // }
                    // println!("{line:?} --- {test_line:?}");
                    intersected
                })
                .count()
                .is_odd()
        });
        boundary_points.chain(inner_points).collect()
    }

    fn plot(&self, include_enclosed: bool) {
        let boundary_box = self.get_boundary_box();
        let points = self.get_points(include_enclosed);

        for y in boundary_box.top_left.y..=boundary_box.bottom_right.y {
            for x in boundary_box.top_left.x..=boundary_box.bottom_right.x {
                let p = Point { x, y };
                if points.contains(&p) {
                    print!("#");
                } else {
                    print!(".");
                }
            }
            println!();
        }
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::{
        event_2023::day18::{Line, Point},
        utils::DayTrait,
    };

    use super::Implementation;

    const EXAMPLE_1: &str = r"
        R 6 (#70c710)
        D 5 (#0dc571)
        L 2 (#5713f0)
        D 2 (#d2c081)
        R 2 (#59c680)
        D 2 (#411b91)
        L 5 (#8ceee2)
        U 2 (#caa173)
        L 1 (#1b58a2)
        U 2 (#caa171)
        R 2 (#7807d2)
        U 3 (#a77fa3)
        L 2 (#015232)
        U 2 (#7a21e3)
    ";

    #[test]
    fn test_line_intersect() {
        let line1 = Line {
            start: Point { x: 0, y: 0 },
            end: Point { x: 10, y: 0 },
        };
        let line2 = Line {
            start: Point { x: 5, y: -1 },
            end: Point { x: 5, y: 1 },
        };
        assert!(line1.intersect(&line2));
        assert!(line2.intersect(&line1));

        let line1 = Line {
            start: Point { x: 0, y: 0 },
            end: Point { x: 10, y: 0 },
        };
        let line2 = Line {
            start: Point { x: 5, y: 2 },
            end: Point { x: 5, y: 1 },
        };
        assert!(!line1.intersect(&line2));
        assert!(!line2.intersect(&line1));

        let line1 = Line {
            start: Point { x: 0, y: 0 },
            end: Point { x: 6, y: 0 },
        };
        let line2 = Line {
            start: Point { x: 4, y: 3 },
            end: Point { x: 4, y: -6 },
        };
        assert!(line1.intersect(&line2));
        assert!(line2.intersect(&line1));
    }

    #[test]
    fn test_day18_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "62");
    }
}
