use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        input
            .trim()
            .lines()
            .map(str::trim)
            .map(|line| {
                line.split(' ')
                    .map(|c| c.parse::<i64>().unwrap())
                    .collect::<Vec<_>>()
            })
            .map(|numbers| get_next_value(&numbers))
            .sum::<i64>()
            .to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        input
            .trim()
            .lines()
            .map(str::trim)
            .map(|line| {
                line.split(' ')
                    .map(|c| c.parse::<i64>().unwrap())
                    .collect::<Vec<_>>()
            })
            .map(|numbers| get_previous_value(&numbers))
            .sum::<i64>()
            .to_string()
    }
}

fn get_next_value(iter: &[i64]) -> i64 {
    let windows = iter.windows(2);
    let diff = windows.map(|items| items[1] - items[0]).collect::<Vec<_>>();
    if diff.iter().all(|n| *n == 0) {
        return *iter.last().unwrap();
    }

    let next = get_next_value(&diff) + iter.last().unwrap();
    next
}

fn get_previous_value(iter: &[i64]) -> i64 {
    let windows = iter.windows(2);
    let diff = windows.map(|items| items[1] - items[0]).collect::<Vec<_>>();
    if diff.iter().all(|n| *n == 0) {
        return *iter.first().unwrap();
    }

    let next = iter.first().unwrap() - get_previous_value(&diff);
    next
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        0 3 6 9 12 15
        1 3 6 10 15 21
        10 13 16 21 30 45
    ";

    #[test]
    fn test_day9_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "114");
    }

    #[test]
    fn test_day9_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "2");
    }
}
