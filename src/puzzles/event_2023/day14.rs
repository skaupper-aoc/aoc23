use std::{collections::HashSet, str::FromStr};

use itertools::Itertools;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation {
    cycles: u32,
}

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let mut map = Map::from_str(input).unwrap();
        map.tilt(Direction::North);

        let load = map.calculate_load();
        load.to_string()
    }

    fn init_part2(&mut self) {
        self.cycles = 1000000000;
    }

    fn call_part2(&self, input: &str) -> String {
        let mut map = Map::from_str(input).unwrap();

        let (periods, start) = map.gather_cycle_period();
        println!("Period length: {}", periods.len());

        let index = (self.cycles as usize - start) % (periods.len() - start) + start;
        map.round_rocks = periods[index].clone();

        let load = map.calculate_load();
        load.to_string()
    }
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
enum Tile {
    Empty,
    CubeRock,
    RoundRock,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Point {
    x: usize,
    y: usize,
}

#[derive(PartialEq, Eq, Clone)]
struct Map {
    orig_lines: Vec<Vec<Tile>>,
    round_rocks: HashSet<Point>,
    width: usize,
    height: usize,
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let orig_lines = s
            .trim()
            .lines()
            .map(|line| {
                line.trim()
                    .chars()
                    .map(|ch| match ch {
                        '.' => Ok(Tile::Empty),
                        '#' => Ok(Tile::CubeRock),
                        'O' => Ok(Tile::RoundRock),
                        _ => Err("Cannot parse tile"),
                    })
                    .collect::<Result<Vec<_>, _>>()
            })
            .collect::<Result<Vec<_>, _>>()?;
        let round_rocks =
            HashSet::from_iter(orig_lines.iter().enumerate().flat_map(|(y, line)| {
                line.iter()
                    .enumerate()
                    .filter(|(_, t)| **t == Tile::RoundRock)
                    .map(move |(x, _)| Point { x, y })
                    .collect::<Vec<_>>()
            }));

        let height = orig_lines.len();
        if height == 0 {
            return Err("Map does not have a single line");
        }
        let width = orig_lines[0].len();

        Ok(Map {
            orig_lines,
            round_rocks,
            width,
            height,
        })
    }
}

#[derive(Clone, Copy)]
enum Direction {
    North,
    East,
    West,
    South,
}

impl Map {
    fn gather_cycle_period(&mut self) -> (Vec<HashSet<Point>>, usize) {
        let mut states = Vec::new();

        while !states.contains(&self.round_rocks) {
            states.push(self.round_rocks.clone());
            self.tilt_cycle();
        }

        let cycle_start = states
            .iter()
            .enumerate()
            .find_map(|(idx, points)| {
                if points == &self.round_rocks {
                    Some(idx)
                } else {
                    None
                }
            })
            .unwrap();
        (states, cycle_start)
    }

    fn tilt_cycle(&mut self) {
        self.tilt(Direction::North);
        self.tilt(Direction::West);
        self.tilt(Direction::South);
        self.tilt(Direction::East);
    }

    fn tilt(&mut self, dir: Direction) {
        // Ugly af...
        let range: Box<dyn Iterator<Item = (usize, usize)>> = match dir {
            Direction::North => Box::new((0..self.width).cartesian_product(0..self.height)),
            Direction::South => Box::new((0..self.width).cartesian_product((0..self.height).rev())),
            Direction::East => Box::new(((0..self.width).rev()).cartesian_product(0..self.height)),
            Direction::West => Box::new((0..self.width).cartesian_product(0..self.height)),
        };
        let get_rev_range: Box<dyn Fn(Point) -> Box<dyn Iterator<Item = Point>>> = match dir {
            Direction::North => {
                Box::new(|p: Point| Box::new((0..p.y).map(move |y| Point { x: p.x, y }).rev()))
            }
            Direction::South => Box::new(|p: Point| {
                Box::new(((p.y + 1)..self.height).map(move |y| Point { x: p.x, y }))
            }),
            Direction::East => Box::new(|p: Point| {
                Box::new(((p.x + 1)..self.width).map(move |x| Point { x, y: p.y }))
            }),
            Direction::West => {
                Box::new(|p: Point| Box::new((0..p.x).map(move |x| Point { x, y: p.y }).rev()))
            }
        };
        let unblock_point: Box<dyn Fn(&Point, Option<Point>) -> Point> = match dir {
            Direction::North => Box::new(|p: &Point, blocking_point: Option<Point>| {
                if let Some(rock) = blocking_point {
                    Point {
                        x: p.x,
                        y: rock.y + 1,
                    }
                } else {
                    Point { x: p.x, y: 0 }
                }
            }),
            Direction::South => Box::new(|p: &Point, blocking_point: Option<Point>| {
                if let Some(rock) = blocking_point {
                    Point {
                        x: p.x,
                        y: rock.y - 1,
                    }
                } else {
                    Point {
                        x: p.x,
                        y: self.height - 1,
                    }
                }
            }),
            Direction::East => Box::new(|p: &Point, blocking_point: Option<Point>| {
                if let Some(rock) = blocking_point {
                    Point {
                        x: rock.x - 1,
                        y: p.y,
                    }
                } else {
                    Point {
                        x: self.width - 1,
                        y: p.y,
                    }
                }
            }),
            Direction::West => Box::new(|p: &Point, blocking_point: Option<Point>| {
                if let Some(rock) = blocking_point {
                    Point {
                        x: rock.x + 1,
                        y: p.y,
                    }
                } else {
                    Point { x: 0, y: p.y }
                }
            }),
        };

        range.map(|(x, y)| Point { x, y }).for_each(|p| {
            if !self.round_rocks.contains(&p) {
                return;
            }

            let blocked_point = get_rev_range(p).find(|rev| {
                self.round_rocks.contains(&Point { x: rev.x, y: rev.y })
                    || self.orig_lines[rev.y][rev.x] == Tile::CubeRock
            });

            self.round_rocks.remove(&p);
            self.round_rocks.insert(unblock_point(&p, blocked_point));
        });
    }

    fn calculate_load(&self) -> u32 {
        self.round_rocks
            .iter()
            .map(|point| (self.height - point.y))
            .map(|l| l as u32)
            .sum::<u32>()
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        O....#....
        O.OO#....#
        .....##...
        OO.#O....O
        .O.....O#.
        O.#..O.#.#
        ..O..#O..O
        .......O..
        #....###..
        #OO..#....
    ";

    const EXAMPLE_2: &str = "
        OOOO.#.O..
        OO..#....#
        OO..O##..O
        O..#.OO...
        ........#.
        ..#....#.#
        ..O..#.O.O
        ..O.......
        #....###..
        #....#....
    ";

    #[test]
    fn test_day14_part1_example1() {
        let implementation = Implementation::default();
        assert_eq!(implementation.call_part1(EXAMPLE_1), "136");
    }

    #[test]
    fn test_day14_part1_example2() {
        let implementation = Implementation::default();
        assert_eq!(implementation.call_part1(EXAMPLE_2), "136");
    }

    #[test]
    fn test_day14_part2_example1() {
        let mut implementation = Implementation::default();
        implementation.init_part2();
        assert_eq!(implementation.call_part2(EXAMPLE_1), "64");
    }
}
