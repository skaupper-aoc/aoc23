use super::utils::DayTrait;

pub(crate) mod day1;
pub(crate) mod day2;
pub(crate) mod day3;
pub(crate) mod day4;
pub(crate) mod day5;
pub(crate) mod day6;
pub(crate) mod day7;
pub(crate) mod day8;
pub(crate) mod day9;

pub(crate) mod day10;
pub(crate) mod day11;
pub(crate) mod day12;
pub(crate) mod day13;
pub(crate) mod day14;
pub(crate) mod day15;
pub(crate) mod day16;
pub(crate) mod day17;
pub(crate) mod day18;
pub(crate) mod day19;

pub(crate) mod day20;
pub(crate) mod day21;
pub(crate) mod day22;
pub(crate) mod day23;
pub(crate) mod day24;

pub fn get_day(day: u8) -> Box<dyn DayTrait> {
    match day {
        1 => Box::<day1::Implementation>::default(),
        2 => Box::<day2::Implementation>::default(),
        3 => Box::<day3::Implementation>::default(),
        4 => Box::<day4::Implementation>::default(),
        5 => Box::<day5::Implementation>::default(),
        6 => Box::<day6::Implementation>::default(),
        7 => Box::<day7::Implementation>::default(),
        8 => Box::<day8::Implementation>::default(),
        9 => Box::<day9::Implementation>::default(),
        10 => Box::<day10::Implementation>::default(),
        11 => Box::<day11::Implementation>::default(),
        12 => Box::<day12::Implementation>::default(),
        13 => Box::<day13::Implementation>::default(),
        14 => Box::<day14::Implementation>::default(),
        15 => Box::<day15::Implementation>::default(),
        16 => Box::<day16::Implementation>::default(),
        17 => Box::<day17::Implementation>::default(),
        18 => Box::<day18::Implementation>::default(),
        19 => Box::<day19::Implementation>::default(),
        20 => Box::<day20::Implementation>::default(),
        21 => Box::<day21::Implementation>::default(),
        22 => Box::<day22::Implementation>::default(),
        23 => Box::<day23::Implementation>::default(),
        24 => Box::<day24::Implementation>::default(),
        _ => panic!("Unknown day number {}", day),
    }
}
