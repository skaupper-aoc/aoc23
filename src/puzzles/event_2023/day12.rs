use std::{collections::HashMap, str::FromStr};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let rows: Vec<Row> = input
            .trim()
            .lines()
            .map(str::trim)
            .map(Row::from_str)
            .map(Result::unwrap)
            .collect();
        let possibilities: Vec<u64> = rows.iter().map(|r| r.count_possibilities()).collect();
        possibilities.iter().sum::<u64>().to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let mut rows: Vec<Row> = input
            .trim()
            .lines()
            .map(str::trim)
            .map(Row::from_str)
            .map(Result::unwrap)
            .collect();
        for r in &mut rows {
            r.unfold(5);
        }
        let possibilities: Vec<u64> = rows.iter().map(|r| r.count_possibilities()).collect();
        possibilities.iter().sum::<u64>().to_string()
    }
}

#[derive(PartialEq, Eq, Debug, Hash, Clone)]
enum SpringCondition {
    Unknown,
    Damaged,
    Operational,
}

#[derive(PartialEq, Eq, Hash)]
struct Row {
    springs: Vec<SpringCondition>,
    consecutive_damaged_groups: Vec<u32>,
}

impl FromStr for Row {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (springs, groups) = s
            .trim()
            .split_once(' ')
            .ok_or("Cannot interpret spring line")?;

        let springs: Vec<SpringCondition> = springs
            .chars()
            .map(|ch| match ch {
                '#' => Ok(SpringCondition::Damaged),
                '.' => Ok(SpringCondition::Operational),
                '?' => Ok(SpringCondition::Unknown),
                _ => Err("Cannot interpret spring condition"),
            })
            .collect::<Result<Vec<_>, _>>()?;
        let groups: Vec<u32> = groups
            .split(',')
            .map(|n| n.parse())
            .collect::<Result<Vec<_>, _>>()
            .map_err(|_| "Cannot interpret string groups")?;

        Ok(Row {
            springs,
            consecutive_damaged_groups: groups,
        })
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum ExpectOperational {
    No,
    Yes,
    Maybe,
}

fn _count_possibilities<'a>(
    row: &'a Row,
    spring_idx: usize,
    group_idx: usize,
    idx_in_group: u32,
    expect_operational: ExpectOperational,
    cache: &mut HashMap<(&'a Row, usize, usize, u32, ExpectOperational), u64>,
) -> u64 {
    let cache_key = (row, spring_idx, group_idx, idx_in_group, expect_operational);
    if cache.contains_key(&cache_key) {
        return cache[&cache_key];
    }

    // If the last group has been finished, check if the remaining springs are all operational
    if group_idx >= row.consecutive_damaged_groups.len() {
        let only_operational_remaining = row.springs[spring_idx..]
            .iter()
            .all(|spring| *spring != SpringCondition::Damaged);
        if only_operational_remaining {
            return 1;
        } else {
            return 0;
        }
    }

    // If the current group is over, start with the next one (with a necessary operational spring beforehand)
    if idx_in_group == row.consecutive_damaged_groups[group_idx] {
        return _count_possibilities(
            row,
            spring_idx,
            group_idx + 1,
            0,
            ExpectOperational::Yes,
            cache,
        );
    }

    // We looked at all springs, but the groups are not over yet
    if spring_idx >= row.springs.len() {
        return 0;
    }

    // When our expectation does not meet the reality, return fast as well
    let curr_spring = &row.springs[spring_idx];
    match (&expect_operational, curr_spring) {
        (ExpectOperational::No, SpringCondition::Operational) => return 0,
        (ExpectOperational::Yes, SpringCondition::Damaged) => return 0,
        _ => {}
    }

    // Precompute some conditions
    let damaged_or_unknown = matches!(
        curr_spring,
        SpringCondition::Damaged | SpringCondition::Unknown
    );
    let operational_or_unknown = matches!(
        curr_spring,
        SpringCondition::Operational | SpringCondition::Unknown
    );
    let cannot_be_damaged =
        *curr_spring == SpringCondition::Unknown && expect_operational == ExpectOperational::Yes;
    let cannot_be_operational =
        *curr_spring == SpringCondition::Unknown && expect_operational == ExpectOperational::No;

    // Depending on the current spring condition, call the function once or twice recursively
    let mut possibilities = 0;
    possibilities += if operational_or_unknown && !cannot_be_operational {
        _count_possibilities(
            row,
            spring_idx + 1,
            group_idx,
            idx_in_group,
            ExpectOperational::Maybe,
            cache,
        )
    } else {
        0
    };

    possibilities += if damaged_or_unknown && !cannot_be_damaged {
        _count_possibilities(
            row,
            spring_idx + 1,
            group_idx,
            idx_in_group + 1,
            ExpectOperational::No,
            cache,
        )
    } else {
        0
    };

    cache.insert(cache_key, possibilities);
    possibilities
}

impl Row {
    fn count_possibilities(&self) -> u64 {
        let mut cache = HashMap::<(&Row, usize, usize, u32, ExpectOperational), u64>::new();
        _count_possibilities(self, 0, 0, 0, ExpectOperational::Maybe, &mut cache)
    }

    fn unfold(&mut self, factor: u32) {
        let groups = self.consecutive_damaged_groups.clone();
        self.consecutive_damaged_groups = Vec::new();
        for _ in 0..factor {
            self.consecutive_damaged_groups.append(&mut groups.clone());
        }

        let springs = self.springs.clone();
        self.springs = Vec::new();
        for i in 0..factor {
            self.springs.append(&mut springs.clone());
            if i != factor - 1 {
                self.springs.push(SpringCondition::Unknown);
            }
        }
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        ???.### 1,1,3
        .??..??...?##. 1,1,3
        ?#?#?#?#?#?#?#? 1,3,1,6
        ????.#...#... 4,1,1
        ????.######..#####. 1,6,5
        ?###???????? 3,2,1
    ";

    const EXAMPLE_2: &str = "
        ?###???????? 3,2,1
    ";

    #[test]
    fn test_day12_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "21");
    }

    #[test]
    fn test_day12_part1_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_2), "10");
    }

    #[test]
    fn test_day12_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "525152");
    }

    #[test]
    fn test_day12_part2_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_2), "506250");
    }
}
