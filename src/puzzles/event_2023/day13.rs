use std::{collections::HashSet, hash::Hash, str::FromStr};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let map: Vec<Map> = input
            .trim()
            .split("\n\n")
            .map(|m| Map::from_str(m).unwrap())
            .collect();

        let sum = map
            .iter()
            .enumerate()
            .map(|(idx, m)| {
                let refl = m.get_reflection_line(0);
                if refl.is_none() {
                    dbg!(idx);
                }
                refl
            })
            .map(Option::unwrap)
            .map(|(idx, refl)| match refl {
                ReflectionLine::Horizontal => 100 * idx,
                ReflectionLine::Vertical => idx,
            })
            .sum::<u32>();

        sum.to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let map: Vec<Map> = input
            .trim()
            .split("\n\n")
            .map(|m| Map::from_str(m).unwrap())
            .collect();

        let sum = map
            .iter()
            .enumerate()
            .map(|(idx, m)| {
                let refl = m.get_reflection_line(1);
                if refl.is_none() {
                    dbg!(idx);
                }
                refl
            })
            .map(Option::unwrap)
            .map(|(idx, refl)| match refl {
                ReflectionLine::Horizontal => 100 * idx,
                ReflectionLine::Vertical => idx,
            })
            .sum::<u32>();

        sum.to_string()
    }
}

#[derive(PartialEq, Eq, Hash, Clone, Copy)]
enum Tile {
    Ash,
    Rock,
}

#[derive(PartialEq, Eq, Hash, Clone)]
struct Map {
    lines: Vec<Vec<Tile>>,
    width: usize,
    height: usize,
}

enum ReflectionLine {
    Vertical,
    Horizontal,
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lines = s
            .trim()
            .lines()
            .map(|line| {
                line.trim()
                    .chars()
                    .map(|ch| match ch {
                        '.' => Ok(Tile::Ash),
                        '#' => Ok(Tile::Rock),
                        _ => Err("Cannot parse tile"),
                    })
                    .collect::<Result<Vec<_>, _>>()
            })
            .collect::<Result<Vec<_>, _>>()?;

        let height = lines.len();
        if height == 0 {
            return Err("Map does not have a single line");
        }
        let width = lines[0].len();

        Ok(Map {
            lines,
            width,
            height,
        })
    }
}

impl Map {
    fn get_reflection_line(&self, max_differences: u32) -> Option<(u32, ReflectionLine)> {
        let rows = self
            .lines
            .iter()
            .map(|line| {
                line.iter()
                    .enumerate()
                    .filter_map(|(idx, t)| {
                        if matches!(t, Tile::Rock) {
                            Some(idx)
                        } else {
                            None
                        }
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        let opt = get_similar_line(&rows, max_differences);
        if let Some(idx) = opt {
            return Some((idx as u32, ReflectionLine::Horizontal));
        }

        let cols = (0..self.width)
            .map(|idx| {
                self.lines
                    .iter()
                    .map(move |line| line[idx])
                    .collect::<Vec<_>>()
            })
            .map(|line| {
                line.iter()
                    .enumerate()
                    .filter_map(|(idx, t)| {
                        if matches!(t, Tile::Rock) {
                            Some(idx)
                        } else {
                            None
                        }
                    })
                    .collect::<Vec<_>>()
            })
            .collect::<Vec<_>>();
        let opt = get_similar_line(&cols, max_differences);
        if let Some(idx) = opt {
            return Some((idx as u32, ReflectionLine::Vertical));
        }

        None
    }
}

fn count_differences<T: Eq + Hash>(iter1: &[T], iter2: &[T]) -> u32 {
    let set1: HashSet<_> = HashSet::from_iter(iter1.iter());
    let set2: HashSet<_> = HashSet::from_iter(iter2.iter());
    set1.symmetric_difference(&set2).count() as u32
}

fn get_similar_line<T: Eq + Hash>(iterable: &Vec<Vec<T>>, differences: u32) -> Option<usize> {
    for idx in 1..iterable.len() {
        let mut found = true;
        let mut diffs_remaining = differences;

        for offset in 0..(iterable.len() - idx) {
            if offset + 1 > idx {
                break;
            }
            if idx + offset >= iterable.len() {
                break;
            }

            let diffs = count_differences(&iterable[idx + offset], &iterable[idx - offset - 1]);
            if diffs > diffs_remaining {
                found = false;
                break;
            }
            diffs_remaining -= diffs;
        }

        if found && diffs_remaining == 0 {
            return Some(idx);
        }
    }

    None
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        #.##..##.
        ..#.##.#.
        ##......#
        ##......#
        ..#.##.#.
        ..##..##.
        #.#.##.#.

        #...##..#
        #....#..#
        ..##..###
        #####.##.
        #####.##.
        ..##..###
        #....#..#
    ";

    const EXAMPLE_2: &str = "
        .####.#...##.#.
        ...###.##.#.#..
        ...###.##.#.#..
        .####.#...#..#.
        ..##......#..#.
        #.#...#.##...##
        .#.#.#..##..##.
        #..##...#####.#
        .#.#.#.#..##.#.
        .....###.#.#..#
        .....###.#.#..#
    ";

    #[test]
    fn test_day13_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "405");
    }

    #[test]
    fn test_day13_part1_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_2), "1000");
    }

    #[test]
    fn test_day13_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "400");
    }
}
