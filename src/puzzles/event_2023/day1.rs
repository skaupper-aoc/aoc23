use std::collections::HashMap;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let mut sum = 0;

        let trimmed_non_empty_lines = input.lines().map(str::trim).filter(|s| !s.is_empty());
        for line in trimmed_non_empty_lines {
            let digits: Vec<_> = line.chars().filter_map(|c| c.to_digit(10)).collect();
            assert!(!digits.is_empty());

            let first = digits.first().unwrap();
            let last = digits.last().unwrap();
            let number = first * 10 + last;
            sum += number;
        }

        sum.to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let numbers: HashMap<&str, u32> = HashMap::from([
            ("zero", 0),
            ("one", 1),
            ("two", 2),
            ("three", 3),
            ("four", 4),
            ("five", 5),
            ("six", 6),
            ("seven", 7),
            ("eight", 8),
            ("nine", 9),
        ]);

        let mut sum = 0;

        let trimmed_non_empty_lines = input.lines().map(str::trim).filter(|s| !s.is_empty());
        for line in trimmed_non_empty_lines {
            let digits: Vec<_> = line
                .chars()
                .chain((0..5).map(|_| ' '))
                .collect::<Vec<_>>()
                .windows(5)
                .map(String::from_iter)
                .filter_map(|s| {
                    let digit = s[0..=0].parse::<u32>().ok();
                    let string = numbers
                        .keys()
                        .find(|&key| s.starts_with(key))
                        .map(|key| *numbers.get(key).unwrap());

                    digit.or(string)
                })
                .collect();

            assert!(!digits.is_empty());

            let first = *digits.first().unwrap();
            let last = *digits.last().unwrap();
            let number = first * 10 + last;
            sum += dbg!(number);
        }

        sum.to_string()
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet
    ";

    const EXAMPLE_2: &str = "
        two1nine
        eightwothree
        abcone2threexyz
        xtwone3four
        4nineeightseven2
        zoneight234
        7pqrstsixteen
    ";

    #[test]
    fn test_day1_part1_example1() {
        let implementation = Implementation {};
        assert_eq!(implementation.call_part1(EXAMPLE_1), "142");
    }

    #[test]
    fn test_day1_part2_example2() {
        let implementation = Implementation {};
        assert_eq!(implementation.call_part2(EXAMPLE_2), "281");
    }
}
