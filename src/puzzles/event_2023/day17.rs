use std::{collections::HashMap, str::FromStr};

use itertools::Itertools;

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let map = Map::from_str(input.trim()).unwrap();
        let (path, heat_loss) = map.find_shortest_path(
            &Point { x: 0, y: 0 },
            &Point {
                x: (map.width - 1) as i32,
                y: (map.height - 1) as i32,
            },
            0,
            3,
        );
        dbg!(path);

        heat_loss.to_string()
    }
    fn call_part2(&self, input: &str) -> String {
        let map = Map::from_str(input.trim()).unwrap();
        let (path, heat_loss) = map.find_shortest_path(
            &Point { x: 0, y: 0 },
            &Point {
                x: (map.width - 1) as i32,
                y: (map.height - 1) as i32,
            },
            4,
            10,
        );
        dbg!(path);

        heat_loss.to_string()
    }
}

#[derive(PartialEq, Eq, Clone)]
struct Map {
    map: HashMap<Point, u32>,
    width: usize,
    height: usize,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct Vector {
    x: i32,
    y: i32,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct CityBlock {
    pos: Point,
    heat_loss: u32,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Copy)]
struct State {
    pos: Point,
    prev_pos: Point,
    total_heat_loss: u32,
    straights: u32,
}

impl std::ops::Add<Vector> for Point {
    type Output = Point;

    fn add(self, rhs: Vector) -> Self::Output {
        Point {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}
impl std::ops::Sub<Point> for Point {
    type Output = Vector;

    fn sub(self, rhs: Point) -> Self::Output {
        let dx = (self.x as i64) - (rhs.x as i64);
        let dy = (self.y as i64) - (rhs.y as i64);
        Vector {
            x: dx as i32,
            y: dy as i32,
        }
    }
}

impl Vector {
    fn abs(&self) -> i32 {
        self.x.abs() + self.y.abs()
    }
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let map = HashMap::from_iter(s.trim().lines().enumerate().flat_map(|(y, line)| {
            line.trim().chars().enumerate().map(move |(x, ch)| {
                (
                    Point {
                        x: x as i32,
                        y: y as i32,
                    },
                    ch.to_digit(10).unwrap(),
                )
            })
        }));

        let height = map.keys().map(|p| p.y).max().unwrap() + 1;
        let width = map.keys().map(|p| p.x).max().unwrap() + 1;

        Ok(Map {
            map,
            width: width as usize,
            height: height as usize,
        })
    }
}

impl State {
    fn neighbours(&self, map: &Map, min_straight: u32, max_straight: u32) -> Vec<(State, u32)> {
        let x_diff = -1..=1;
        let y_diff = -1..=1;

        x_diff
            .cartesian_product(y_diff)
            .map(|(dx, dy)| Vector { x: dx, y: dy })
            .filter_map(|vec| {
                if vec.abs() != 1 {
                    return None;
                }
                let new_pos = self.pos + vec;
                if new_pos.x >= map.width as i32
                    || new_pos.y >= map.height as i32
                    || new_pos.x < 0
                    || new_pos.y < 0
                {
                    return None;
                }
                if new_pos == self.prev_pos {
                    return None;
                }

                let cost = map.map[&new_pos];

                let old_vec = self.pos - self.prev_pos;
                if old_vec == vec {
                    if self.straights == max_straight {
                        return None;
                    }
                    return Some((
                        State {
                            pos: new_pos,
                            prev_pos: self.pos,
                            total_heat_loss: self.total_heat_loss + cost,
                            straights: self.straights + 1,
                        },
                        cost,
                    ));
                }
                if self.straights < min_straight {
                    return None;
                }

                Some((
                    State {
                        pos: new_pos,
                        prev_pos: self.pos,
                        total_heat_loss: self.total_heat_loss + cost,
                        straights: 1,
                    },
                    cost,
                ))
            })
            .collect()
    }
}

impl Map {
    fn find_shortest_path(
        &self,
        start: &Point,
        end: &Point,
        min_straight: u32,
        max_straight: u32,
    ) -> (Vec<Point>, u32) {
        let state = State {
            pos: *start,
            prev_pos: Point {
                x: start.x - 1,
                y: start.y,
            },
            total_heat_loss: self.map[start],
            straights: 0,
        };

        let result = pathfinding::directed::dijkstra::dijkstra(
            &state,
            |state| state.neighbours(self, min_straight, max_straight),
            // |state| (state.pos.y.abs_diff(end.y) + state.pos.x.abs_diff(end.x)),
            |state| {
                state.pos == *end
                    && state.straights >= min_straight
                    && state.straights <= max_straight
            },
        );

        let (path, heat_loss) = result.unwrap();
        (path.iter().map(|state| state.pos).collect(), heat_loss)
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = r"
        2413432311323
        3215453535623
        3255245654254
        3446585845452
        4546657867536
        1438598798454
        4457876987766
        3637877979653
        4654967986887
        4564679986453
        1224686865563
        2546548887735
        4322674655533
    ";

    const EXAMPLE_2: &str = r"
        111111111111
        999999999991
        999999999991
        999999999991
        999999999991
    ";

    #[test]
    fn test_day17_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "102");
    }

    #[test]
    fn test_day17_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "94");
    }

    #[test]
    fn test_day17_part2_example2() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_2), "71");
    }
}
