use itertools::Itertools;
use std::{collections::HashMap, ops::Range};

use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        let mut groups = input.trim().split("\n\n").map(str::trim);
        let seeds: Vec<_> = groups
            .next()
            .unwrap()
            .strip_prefix("seeds:")
            .expect("the first line must start with 'seeds:'")
            .trim()
            .split(' ')
            .map(|seed| seed.parse::<u64>().expect("failed to parse seed number"))
            .collect();

        let transform_map: HashMap<&str, (&str, HashMap<Range<u64>, i64>)> =
            HashMap::from_iter(groups.map(|block| {
                let mut lines = block.lines().map(str::trim);
                let map_str = lines.next().unwrap();

                let (map_str, remainder) = map_str
                    .split_once(' ')
                    .expect("cannot parse mapping header");

                let _ = remainder.strip_prefix("map:").unwrap();
                let (source, dest) = map_str
                    .split_once("-to-")
                    .expect("cannot parse mapping string");

                let mappings = HashMap::from_iter(lines.map(|instr| {
                    let (dest_start, source_start, rng_length) = instr
                        .splitn(3, ' ')
                        .map(|s| s.parse::<u64>().unwrap())
                        .next_tuple()
                        .unwrap();

                    (
                        (source_start..(source_start + rng_length)),
                        (dbg!(dest_start as i64) - dbg!(source_start as i64)),
                    )
                }));

                (source, (dest, mappings))
            }));

        let do_location_lookup = |seed| {
            let mut source = "seed";
            let mut nr = seed as i64;

            while let Some((dest, map)) = transform_map.get(source) {
                source = dest;
                let key = map.keys().find(|key| key.contains(&(nr as u64)));
                if let Some(key) = key {
                    nr += *map.get(key).unwrap();
                }

                if source == "location" {
                    break;
                }
            }

            nr
        };

        let min_loc_number = seeds.into_iter().map(do_location_lookup).min().unwrap();
        min_loc_number.to_string()
    }

    fn call_part2(&self, input: &str) -> String {
        let mut groups = input.trim().split("\n\n").map(str::trim);
        let seeds: Vec<_> = groups
            .next()
            .unwrap()
            .strip_prefix("seeds:")
            .expect("the first line must start with 'seeds:'")
            .trim()
            .split(' ')
            .map(|seed| seed.parse::<u64>().expect("failed to parse seed number"))
            .chunks(2)
            .into_iter()
            .flat_map(|mut chunk| {
                let (start, len) = chunk.next_tuple().unwrap();
                start..(start + len)
            })
            .collect();

        let transform_map: HashMap<&str, (&str, HashMap<Range<u64>, i64>)> =
            HashMap::from_iter(groups.map(|block| {
                let mut lines = block.lines().map(str::trim);
                let map_str = lines.next().unwrap();

                let (map_str, remainder) = map_str
                    .split_once(' ')
                    .expect("cannot parse mapping header");

                let _ = remainder.strip_prefix("map:").unwrap();
                let (source, dest) = map_str
                    .split_once("-to-")
                    .expect("cannot parse mapping string");

                let mappings = HashMap::from_iter(lines.map(|instr| {
                    let (dest_start, source_start, rng_length) = instr
                        .splitn(3, ' ')
                        .map(|s| s.parse::<u64>().unwrap())
                        .next_tuple()
                        .unwrap();

                    (
                        (source_start..(source_start + rng_length)),
                        (dbg!(dest_start as i64) - dbg!(source_start as i64)),
                    )
                }));

                (source, (dest, mappings))
            }));

        let do_location_lookup = |seed| {
            let mut source = "seed";
            let mut nr = seed as i64;

            while let Some((dest, map)) = transform_map.get(source) {
                source = dest;
                let key = map.keys().find(|key| key.contains(&(nr as u64)));
                if let Some(key) = key {
                    nr += *map.get(key).unwrap();
                }

                if source == "location" {
                    break;
                }
            }

            nr
        };

        let min_loc_number = seeds.into_iter().map(do_location_lookup).min().unwrap();
        min_loc_number.to_string()
    }
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        seeds: 79 14 55 13

        seed-to-soil map:
        50 98 2
        52 50 48

        soil-to-fertilizer map:
        0 15 37
        37 52 2
        39 0 15

        fertilizer-to-water map:
        49 53 8
        0 11 42
        42 0 7
        57 7 4

        water-to-light map:
        88 18 7
        18 25 70

        light-to-temperature map:
        45 77 23
        81 45 19
        68 64 13

        temperature-to-humidity map:
        0 69 1
        1 0 69

        humidity-to-location map:
        60 56 37
        56 93 4
    ";

    #[test]
    fn test_day5_part1_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part1(EXAMPLE_1), "35");
    }

    #[test]
    fn test_day5_part2_example1() {
        let implementation = Implementation;
        assert_eq!(implementation.call_part2(EXAMPLE_1), "46");
    }
}
