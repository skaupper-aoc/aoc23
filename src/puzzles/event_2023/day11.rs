use std::{collections::HashMap, str::FromStr};

use crate::utils::DayTrait;

use itertools::Itertools;

#[derive(Default)]
pub struct Implementation {
    expansion_factor: usize,
}

impl DayTrait for Implementation {
    fn init_part1(&mut self) {
        self.expansion_factor = 2;
    }

    fn call_part1(&self, input: &str) -> String {
        let mut map = Map::from_str(input).unwrap();
        map.expand(self.expansion_factor);
        let shortest_paths = map.get_pairwise_shortest_paths();
        shortest_paths.values().sum::<usize>().to_string()
    }

    fn init_part2(&mut self) {
        self.expansion_factor = 1000000;
    }

    fn call_part2(&self, input: &str) -> String {
        let mut map = Map::from_str(input).unwrap();
        map.expand(self.expansion_factor);
        let shortest_paths = map.get_pairwise_shortest_paths();
        shortest_paths.values().sum::<usize>().to_string()
    }
}

struct Map {
    galaxies: Vec<Point>,
    width: usize,
    height: usize,
}

impl FromStr for Map {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let galaxies: Vec<_> = s
            .trim()
            .lines()
            .map(str::trim)
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .filter(|(_, ch)| *ch == '#')
                    .map(move |(x, _)| Point { x, y })
            })
            .collect();

        let width = galaxies.iter().map(|p| p.x).max().unwrap();
        let height = galaxies.iter().map(|p| p.y).max().unwrap();

        Ok(Map {
            galaxies,
            width,
            height,
        })
    }
}

impl Map {
    fn expand(&mut self, factor: usize) {
        let mut x = 0;
        while x < self.width {
            if !self.galaxies.iter().any(|point| point.x == x) {
                self.galaxies
                    .iter_mut()
                    .filter(|point| point.x > x)
                    .for_each(|point| point.x += factor - 1);
                x += factor - 1;
                self.width += factor - 1;
            }
            x += 1;
        }

        let mut y = 0;
        while y < self.height {
            if !self.galaxies.iter().any(|point| point.y == y) {
                self.galaxies
                    .iter_mut()
                    .filter(|point| point.y > y)
                    .for_each(|point| point.y += factor - 1);
                y += factor - 1;
                self.height += factor - 1;
            }
            y += 1;
        }
    }

    fn get_pairwise_shortest_paths(&self) -> HashMap<(&Point, &Point), usize> {
        HashMap::from_iter(self.galaxies.iter().combinations(2).map(|points| {
            (
                (points[0], points[1]),
                (points[1].x.abs_diff(points[0].x) + points[1].y.abs_diff(points[0].y)),
            )
        }))
    }
}

#[derive(Debug, Hash, PartialEq, Eq)]
struct Point {
    x: usize,
    y: usize,
}

#[cfg(test)]
mod test {
    use crate::puzzles::utils::DayTrait;

    use super::Implementation;

    const EXAMPLE_1: &str = "
        ...#......
        .......#..
        #.........
        ..........
        ......#...
        .#........
        .........#
        ..........
        .......#..
        #...#.....
    ";

    #[test]
    fn test_day11_part1_example1() {
        let implementation = Implementation {
            expansion_factor: 2,
        };
        assert_eq!(implementation.call_part1(EXAMPLE_1), "374");
    }

    #[test]
    fn test_day11_part2_example1_fac10() {
        let implementation = Implementation {
            expansion_factor: 10,
        };
        assert_eq!(implementation.call_part2(EXAMPLE_1), "1030");
    }

    #[test]
    fn test_day11_part2_example1_fac100() {
        let implementation = Implementation {
            expansion_factor: 100,
        };
        assert_eq!(implementation.call_part2(EXAMPLE_1), "8410");
    }
}
