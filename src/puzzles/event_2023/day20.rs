use crate::utils::DayTrait;

#[derive(Default)]
pub struct Implementation;

impl DayTrait for Implementation {
    fn call_part1(&self, input: &str) -> String {
        input.into()
    }
    fn call_part2(&self, input: &str) -> String {
        input.into()
    }
}
