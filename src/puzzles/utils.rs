pub trait DayTrait {
    fn init_part1(&mut self) {}
    fn call_part1(&self, input: &str) -> String;

    fn init_part2(&mut self) {}
    fn call_part2(&self, input: &str) -> String;
}

pub fn get_day(year: u32, day: u8) -> Box<dyn DayTrait> {
    match year {
        2023 => super::event_2023::get_day(day),
        _ => panic!("Unknown event number {}", &year),
    }
}
